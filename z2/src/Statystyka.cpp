#include "Statystyka.hh"

/*!
 * Wyswietla statystyki testu.
 * Argumenty:
 *    ST - struktura przechowujaca dane statystyczne.
 */
void Wyswietl(Statystyka ST){
    cout<<"Ilość dobrych odpowiedzi:"<<ST.DO<<endl;
    cout<<"Ilość zlych odpowiedzi:"<<ST.ZO<<endl;
    cout<<"Wynik procentowy poprawnych odpowiedzi:"<<(ST.DO*100/(ST.DO+ST.ZO))<<"%"<<endl;
}

/*!
 * Wpisuje do struktury statystyki dobra lub zla ospowiedz.
 * Argumenty:
 *    odp - zmienna przekazujaca odpowiednia wartosc,
 *    ST - referencja do struktury statystyki.
 */
void Odpowiedzi(int odp, Statystyka & ST){
    if (odp == 1)
    {
        ST.DO++;
    }else
    {
        ST.ZO++;
    }
}