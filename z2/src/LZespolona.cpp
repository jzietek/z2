#include "LZespolona.hh"
#include "Statystyka.hh"
using namespace std;


/*!
 * Realizuje dodanie dwoch liczb zespolonych.
 * Argumenty:
 *    Z1 - pierwszy skladnik dodawania,
 *    Z2 - drugi skladnik dodawania.
 * Zwraca:
 *    Sume dwoch skladnikow przekazanych jako parametry.
 */
LZespolona operator+(LZespolona Z1,LZespolona Z2){
    LZespolona Wynik;
    Wynik.im=Z1.im+Z2.im;
    Wynik.re=Z1.re+Z2.re;
    return Wynik;
}

/*!
 * Realizuje odejmowanie dwoch liczb zespolonych.
 * Argumenty:
 *    Z1 - pierwszy skladnik odejmowania,
 *    Z2 - drugi skladnik odejmowania.
 * Zwraca:
 *    Roznice dwoch skladnikow przekazanych jako parametry.
 */
LZespolona operator-(LZespolona Z1,LZespolona Z2){
    LZespolona Wynik;
    Wynik.im=Z1.im-Z2.im;
    Wynik.re=Z1.re-Z2.re;
    return Wynik;
}

/*!
 * Realizuje mnozenie dwoch liczb zespolonych.
 * Argumenty:
 *    Z1 - pierwszy skladnik mnozenia,
 *    Z2 - drugi skladnik mnozenia.
 * Zwraca:
 *    Iloczyn dwoch skladnikow przekazanych jako parametry.
 */
LZespolona operator*(LZespolona Z1,LZespolona Z2){
    LZespolona Wynik;
    Wynik.im=(Z1.re*Z2.im)+(Z1.im*Z2.re);
    Wynik.re=(Z1.re*Z2.re)-(Z1.im*Z2.im);
    return Wynik;
}

/*!
 * Realizuje sprzezenie liczby zespolonej.
 * Argumenty:
 *    Sp - skladnik wyznaczania sprzezenia.
 * Zwraca:
 *    Sprzezenie liczby zespolonej przekazanej jako parametr.
 */
LZespolona Sprzezenie(LZespolona Sp){
    Sp.im=-(Sp.im);
    return Sp;
}

/*!
 * Oblicza modul do kwadratu odpowiedniej liczby zespolonej.
 * Argumenty:
 *    Z2 - skladnik obliczania modulu.
 * Zwraca:
 *    Kwadrat modułu liczby zespolonej przekazanej jako parametr.
 */
double Modul2(LZespolona Z2){
    double Mod;
    Mod=Z2.im*Z2.im+Z2.re*Z2.re;
    return Mod;
}

/*!
 * Realizuje dzielenie liczb zespolonej przed double.
 * Argumenty:
 *    Z1 - pierwszy skladnik dzielenia,
 *    D - drugi skladnik dzielenia.
 * Zwraca:
 *    Iloraz dwoch skladnikow przekazanych jako parametry.
 */
LZespolona operator/(LZespolona Z1, double D){
    if(D!=0){
    Z1.re=(Z1.re/D);
    Z1.im=(Z1.im/D);
    }else
    {
      cerr << "Blad: nie mozna dzielic przez zero." << endl;
    }
    return Z1;
}

/*!
 * Realizuje dzielenie dwoch liczb zespolonych.
 * Argumenty:
 *    Z1 - pierwszy skladnik dzielenia,
 *    Z2 - drugi skladnik dzielenia.
 * Zwraca:
 *    Iloraz dwoch skladnikow przekazanych jako parametry.
 */
LZespolona operator/(LZespolona Z1,LZespolona Z2){
    LZespolona Wynik;
    Wynik=Z1*Sprzezenie(Z2);
    Wynik=Wynik/(Modul2(Z2));
    return Wynik;
}


/*!
 * Wyswietla liczbe zespolona.
 * Argumenty:
 *    wys - wyswietlana liczba zespolona.
 */
void Wyswietl(LZespolona wys){
  wys = Round(wys);
  cout << fixed << "(" << wys.re << showpos << wys.im << noshowpos << "i" << ")" << endl;
  }

/*!
 * Wczytuje liczbe zespolona.
 * Zwraca:
 *    Wczytana liczbe zespolona.
 */
LZespolona WczytajLZ(){
  LZespolona LZ;
  char pom;
  int blad = 0;
  int brakbledu = 0;

  while(blad != 3 && brakbledu != 1){
  cin >> pom;
  if (cin.fail())
  {
    cerr << "Blad: wczytywanie liczby zespolonej nie powiodlo sie" << endl;
    blad++;
    cin.clear( );
    cin.ignore(10000,'\n');
  }else{
  if (pom != '(')
  {
    cerr << "Blad: wczytywanie liczby zespolonej nie powiodlo sie" << endl;
    blad++;
    cin.clear( );
    cin.ignore(10000,'\n');
  }else
  {
    cin >> LZ.re;
    if (cin.fail())
    {
      cerr << "Blad: wczytanie liczby zespolonej nie powiodlo sie" << endl;
      blad++;
      cin.clear( );
      cin.ignore(10000,'\n');
    }else
    {
      cin >> LZ.im;
      if (cin.fail())
      {
        cerr << "Blad: wczytanie liczby zespolonej nie powiodlo sie" << endl;
        blad++;
        cin.clear( );
        cin.ignore(10000,'\n');
      }else
      {
        cin >> pom;
        if (cin.fail())
        {
          cerr << "Blad: wczytywanie liczby zespolonej nie powiodlo sie" << endl;
          blad++;
          cin.clear( );
          cin.ignore(10000,'\n');
        }
        if (pom!='i')
        {
          cerr << "Blad: wczytanie liczby zespolonej nie powiodlo sie" << endl;
          blad++;
          cin.clear( );
          cin.ignore(10000,'\n');
        }else
        {
          cin >> pom;
          if (cin.fail())
          {
            cerr << "Blad: wczytywanie liczby zespolonej nie powiodlo sie" << endl;
            blad++;
            cin.clear( );
            cin.ignore(10000,'\n');
          }
          
          if (pom != ')')
          {
            cerr << "Blad: wczytanie liczby zespolonej nie powiodlo sie" << endl;
            blad++;
            cin.clear( );
            cin.ignore(10000,'\n');
          }else
          {
            brakbledu++;
          }}}}}}}
  
  
if (blad == 3)
  {
    cerr << "Blad: wykorzystana ilosc prob wczytywania liczby zespolonej" << endl;
  }
  return LZ;
  }

/*!
 * Wyswietla liczbe zespolona za pomoca przeciazenia operatora.
 * Argumenty:
 *    StrWyj - strumien wyjsciowy,
 *    Z1 - wyswietlana liczba zespolona.
 * Zwraca:
 *    Referencje do strumienia wyjsciowego.
 */
ostream & operator << (ostream & StrWyj, LZespolona Z1){
  Z1 = Round (Z1);
  StrWyj << fixed << "(" << Z1.re << showpos << Z1.im << noshowpos << "i" << ")" << endl;
  return StrWyj;
}

/*!
 * Wczytuje liczbe zespolona za pomoca przeciazenia operatora.
 * Argumenty:
 *    StrWej - strumien wejsciowy,
 *    Z1 - wczytywana liczba zespolona.
 * Zwraca:
 *    Referencje do strumienia wejsciowego.
 */

istream & operator >> (istream &StrWej, LZespolona & Z1){
  char pom;
  int blad = 0;
  int brakbledu = 0;

  while(blad != 3 && brakbledu != 1){
  StrWej >> pom;
  if (StrWej.fail())
  {
    cerr << "Blad: wczytywanie liczby zespolonej nie powiodlo sie" << endl;
    blad++;
    StrWej.clear( );
    StrWej.ignore(10000,'\n');
  }else{
  if (pom != '(')
  {
    cerr << "Blad: wczytywanie liczby zespolonej nie powiodlo sie" << endl;
    blad++;
    StrWej.clear( );
    StrWej.ignore(10000,'\n');
  }else
  {
    StrWej >> Z1.re;
    if (StrWej.fail())
    {
      cerr << "Blad: wczytanie liczby zespolonej nie powiodlo sie" << endl;
      blad++;
      StrWej.clear( );
      StrWej.ignore(10000,'\n');
    }else
    {
      StrWej >> Z1.im;
      if (StrWej.fail())
      {
        cerr << "Blad: wczytanie liczby zespolonej nie powiodlo sie" << endl;
        blad++;
        StrWej.clear( );
        StrWej.ignore(10000,'\n');
      }else
      {
        StrWej >> pom;
        if (StrWej.fail())
        {
          cerr << "Blad: wczytywanie liczby zespolonej nie powiodlo sie" << endl;
          blad++;
          StrWej.clear( );
          StrWej.ignore(10000,'\n');
        }
        if (pom!='i')
        {
          cerr << "Blad: wczytanie liczby zespolonej nie powiodlo sie" << endl;
          blad++;
          StrWej.clear( );
          StrWej.ignore(10000,'\n');
        }else
        {
          StrWej >> pom;
          if (StrWej.fail())
          {
            cerr << "Blad: wczytywanie liczby zespolonej nie powiodlo sie" << endl;
            blad++;
            StrWej.clear( );
            StrWej.ignore(10000,'\n');
          }
          
          if (pom != ')')
          {
            cerr << "Blad: wczytanie liczby zespolonej nie powiodlo sie" << endl;
            blad++;
            StrWej.clear( );
            StrWej.ignore(10000,'\n');
          }else
          {
            brakbledu++;
          }}}}}}}
  
  
if (blad == 3)
  {
    cerr << "Blad: wykorzystana ilosc prob wczytywania liczby zespolonej" << endl;
  }
  return StrWej;
}

/*
 * Porównuje liczby zespolone za pomoca przeciazenia operatora.
 * Argumenty:
 *    Z1 - pierwszy skladnik porownywania,
 *    Z2 - drugi skladnik porownywania.
 * Zwraca:
 *    Odpowiednia wartosc liczbowa w zaleznosci od porownania.
 */
int operator==(LZespolona Z1, LZespolona Z2){
  if (Z1.re==Z2.re && Z1.im == Z2.im)
  {
    cout << "Poprawna odpowiedz."<<endl<<endl;
    return 1;
  }
  else
  {
    cout << "Niepoprawna odpowiedz."<< endl << endl;
    cout << "Poprawna odpowiedz to: "<< endl;
    Wyswietl(Z1);
    cout << endl;
    return 0;
  }
}

/*!
 * Przybliza wartosc liczby zespolonej obcinajac wszystko co mniejsze od czesci dziesietnych.
 * Argumenty:
 *    LZ - obliczana liczba zespolone.
 * Zwraca:
 *    Wynik przyblizenia - liczbe zespolona.
 */
LZespolona Round(LZespolona LZ)
{
    int re, im;

    LZ.re = LZ.re * 10;
    LZ.im = LZ.im * 10;

    re = LZ.re;
    im = LZ.im;

    LZ.re = re;
    LZ.im = im;

    LZ.re = LZ.re / 10;
    LZ.im = LZ.im / 10;

    return LZ;
}