#include "WyrazenieZesp.hh"
using namespace std;

/*!
 * Wyswietla wyrazenie zespolone.
 * Argumenty:
 *    wyr - wyswietlane wyrazenie zespolone.
 */
void Wyswietl(WyrazenieZesp wyr){
  wyr.Arg1=Round(wyr.Arg1);
  cout << fixed << "(" << wyr.Arg1.re << showpos << wyr.Arg1.im << noshowpos << "i" << ")";
  switch (wyr.Op)
  {
  case 0:
    cout << '+';
    break;
  case 1:
    cout << '-';
    break;
  case 2:
    cout << '*';
    break;
  case 3:
    cout << '/';
    break;
  
  default:
    cout << "Blad: " << endl;
    break;
  }
  Wyswietl(wyr.Arg2);
}

/*!
 * Wczytuje wyrazenie zespolone.
 * Zwraca:
 *    Wczytane wyrazenie zespolone.
 */
WyrazenieZesp WczytajWZ(){
  WyrazenieZesp WZ;
  char pom;

  WZ.Arg1=WczytajLZ();
  cin>>pom;
  switch (pom)
  {
  case '+':
    WZ.Op=Op_Dodaj;
    break;
  case '-':
    WZ.Op=Op_Odejmij;
    break;
  case '*':
    WZ.Op=Op_Mnoz;
    break;
  case '/':
    WZ.Op=Op_Dziel;
    break;
  default:
  cerr << "Blad: wczytanie operatora nie powiodlo sie" << endl;
    break;
  }
  WZ.Arg2=WczytajLZ();
  
  return WZ;
}

/*!
 * Oblicza wyrazenie zespolone.
 * Argumenty:
 *    WZ - obliczane wyrazenie zespolone.
 * Zwraca:
 *    Wynik dzialania - liczbe zespolona.
 */
LZespolona solve(WyrazenieZesp WZ){
  LZespolona Z1;
  WZ.Arg1=Round(WZ.Arg1);
  WZ.Arg2=Round(WZ.Arg2);
  switch (WZ.Op)
  {
  case 0:
  Z1=WZ.Arg1+WZ.Arg2;
    break;
  case 1:
  Z1=WZ.Arg1-WZ.Arg2;
    break;
  case 2:
  Z1=WZ.Arg1*WZ.Arg2;
    break;
  case 3:
  Z1=WZ.Arg1/WZ.Arg2;
    break;
  default:
    break;
  }
  Z1 = Round (Z1);
  return Z1;
}

/*!
 * Wyswietla wyrazenie zespolone za pomoca przeciazenia operatora.
 * Argumenty:
 *    StrWyj - referencja do strumienia,
 *    WZ - wyswietlane wyrazenie zespolone.
 * Zwraca:
 *    Referencje do strumienia wyjsciowego.
 */
ostream & operator << (ostream & StrWyj, WyrazenieZesp WZ){
  WZ.Arg1 = Round (WZ.Arg1);
  StrWyj << fixed << setprecision (1) << "("<< WZ.Arg1.re << showpos << WZ.Arg1.im << noshowpos << "i" << ")";
  switch (WZ.Op)
  {
  case 0:
    StrWyj << '+';
    break;
  case 1:
    StrWyj << '-';
    break;
  case 2:
    StrWyj << '*';
    break;
  case 3:
    StrWyj << '/';
    break;
  }
  StrWyj << WZ.Arg2;
  return StrWyj;
}