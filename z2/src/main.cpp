#include <iostream>
#include "BazaTestu.hh"
#include "LZespolona.hh"
#include "WyrazenieZesp.hh"
#include "Statystyka.hh"

using namespace std;




int main(int argc, char **argv)
{

  if (argc < 2) {
    cout << endl;
    cout << " Brak opcji okreslajacej rodzaj testu." << endl;
    cout << " Dopuszczalne nazwy to:  latwy, trudny." << endl;
    cout << endl;
    return 1;
  }


  BazaTestu   BazaT = { nullptr, 0, 0 };

  if (InicjalizujTest(&BazaT,argv[1]) == false) {
    cerr << " Inicjalizacja testu nie powiodla sie." << endl;
    return 1;
  }

  
  cout << endl;
  cout << " Start testu arytmetyki zespolonej: " << argv[1] << endl;
  cout << endl;

  WyrazenieZesp   WyrZ_PytanieTestowe;
  LZespolona Z1, Z2;
  Statystyka ST;
  int odp;
  
  while (PobierzNastpnePytanie(&BazaT,&WyrZ_PytanieTestowe)) {
    cout << "Oblicz rownanie" << endl;
    cout << WyrZ_PytanieTestowe;
    cout << "Wynik zapisz w okraglych nawiasach." << endl;
    cout << "Obetnij wszystko co mniejsze od czesci dziesietnych." << endl;
    cout << "Wynik: " << endl;
    Z1=solve(WyrZ_PytanieTestowe);
    cin >> Z2;
    odp=Z1==Z2;
    Odpowiedzi(odp, ST);
  }
    Wyswietl(ST);
  
  cout << endl;
  cout << " Koniec testu" << endl;
  cout << endl;

}