#ifndef LZESPOLONA_HH
#define LZESPOLONA_HH
#include<iostream>
#include<cmath>
#include<iomanip>
#include "Statystyka.hh"
using namespace std;
/*!
 *  Plik zawiera definicje struktury LZesplona oraz zapowiedzi
 *  przeciazen operatorow arytmetycznych dzialajacych na tej 
 *  strukturze.
 */


/*!
 * Modeluje pojecie liczby zespolonej
 */
struct  LZespolona {
  double   re;    /*! Pole repezentuje czesc rzeczywista. */
  double   im;    /*! Pole repezentuje czesc urojona. */
};

/*!
 * Prototypy przeciazen operatorow, wyswietlania i 
 * wczytywania liczby zespolonej oraz reszta funkcji
 * zwiazanych z liczba zespolona.
 */

/*
 * Wyswietla liczbe zespolona.
 */
void Wyswietl(LZespolona);

/*
 * Przeciazenie operatora dodawania dla liczb zespolonych.
 */
LZespolona  operator + (LZespolona, LZespolona);

/*
 * Przeciazenie operatora odejmowania dla liczb zespolonych.
 */
LZespolona  operator - (LZespolona, LZespolona);

/*
 * Przeciazenie operatora mnozenia dla liczb zespolonych.
 */
LZespolona  operator * (LZespolona, LZespolona);

/*
 * Przeciazenie operatora dzielenia dla liczb zespolonych.
 */
LZespolona  operator / (LZespolona, LZespolona);

/*
 * Przeciazenie operatora dzielenia liczby zespolonej przed double.
 */
LZespolona operator / (LZespolona Z1, double D);

/*
 * Oblicza modul do kwadratu liczby zespolonej.
 */
double Modul2(LZespolona);

/*
 * Wczytuje liczbe zespolona.
 */
LZespolona WczytajLZ();

/*
 * Przeciazenie operatora wyswietlania dla liczby zespolonej.
 */
ostream & operator << (ostream &StrWyj, LZespolona);

/*
 * Przeciazenie operatora wczytywania liczby zespolonej.
 */
istream & operator >> (istream &StrWej, LZespolona & Z1);

/*
 * Przeciazenie operatora porownania dla liczb zespolonych.
 */
int operator == (LZespolona, LZespolona);

/*
 * Przybliza liczbe zespolona obcinajac wszystko co mniejsze od czesci dziesietnych.
 */
LZespolona Round(LZespolona Z1);

#endif