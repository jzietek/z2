#ifndef WYRAZENIEZESP_HH
#define WYRAZENIEZESP_HH


#include "cmath"
#include "LZespolona.hh"
#include "Statystyka.hh"


/*!
 * Modeluje zbior operatorow arytmetycznych.
 */
enum Operator { Op_Dodaj, Op_Odejmij, Op_Mnoz, Op_Dziel };



/*
 * Modeluje pojecie dwuargumentowego wyrazenia zespolonego
 */
struct WyrazenieZesp {
  LZespolona   Arg1;   // Pierwszy argument wyrazenia arytmetycznego
  Operator     Op;     // Opertor wyrazenia arytmetycznego
  LZespolona   Arg2;   // Drugi argument wyrazenia arytmetycznego
};


/*
 * Wyswietla wyrazenie zespolone.
 */
void Wyswietl(WyrazenieZesp  WyrZ);

/*
 * Wczytuje wyrazenie zespolone.
 */
WyrazenieZesp WczytajWZ();

/*
 * Oblicza wyrazenie zespolone.
 */
LZespolona solve(WyrazenieZesp);

/*
 * Przeciazenie operatora wyswietlania dla wyrazenia zespolonego.
 */
ostream & operator << (ostream & StrWyj, WyrazenieZesp);

#endif
