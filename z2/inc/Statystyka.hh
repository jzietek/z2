#ifndef STATYSTYKA_HH
#define STATYSTYKA_HH
#include <iostream>
using namespace std;

/*
 * Przechowuje informacje o dobrych i zlych odpowiedziach.
 */
struct Statystyka{
    int DO = 0;        // dobre odpowiedzi
    int ZO = 0;       // zle odpowiedzi
};

/*
 * Wyswietla statystyki testu.
 */
void Wyswietl(Statystyka);

/*
 * Wpisuje odpowiednie dane do statystyki.
 */
void Odpowiedzi(int, Statystyka &);
#endif